var app = angular.module('appAccountNumber', ['ngTable','angularjs-crypto']);

app.run(function(cfCryptoHttpInterceptor, $rootScope) {
    $rootScope.base64Key = CryptoJS.enc.Base64.parse("0123456789abcdef0123456789abcdef");
	$rootScope.iv = CryptoJS.enc.Base64.parse("3ad77bb90d7a3770a89ecaf32466ef97");
})

app.controller('ctrlAccountNumber', function ($scope, $rootScope , $http, $filter, NgTableParams) {

    this.myDate = new Date();
    this.isOpen = false;

    $scope.members = [];
    $scope.expiredmembers = [];
    $scope.selected = {};
    $scope.objmember = {};
    $scope.objregister = {};
    $scope.memberid = 0;
    $scope.objprofile = {};
    $scope.viewby = 10;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 5; //Number of pager buttons to show
    $scope.filter = "";
    $scope.state = [];
    $scope.country = [];
    $scope.relations = [];
    $scope.rank = [];
    $scope.confirm = [];
    $scope.objregister.genderid = 0;
    $scope.servicenovalid = '';
    $scope.DisableButton = false;
    $scope.isDisabled = false;
    $scope.regionDetails = [];
    $scope.TalukDetails = [];
    // Get character
    $scope.character = [];
    $scope.servicenumber = 0;

    // Get members
    debugger
    $scope.servicenumber = localStorage.getItem('MemberSearchValue');
    if (Number($scope.servicenumber) == 0) {
        $scope.servicenumber = 0;
        $scope.objmember.search_val = '';
    } else {
        $scope.objmember.search_val = $scope.servicenumber;
        localStorage.setItem('MemberSearchValue', 0);

        $scope.source_string  = $scope.objmember.search_val.toString();   
		console.log('$scope.source_string',$scope.source_string);
        var encrypted = CryptoJS.AES.encrypt($scope.source_string,$rootScope.base64Key,{ iv: $rootScope.iv });
        

        console.log('ciphertext = ' + encrypted);  
        
        $http({
            method: 'GET', 
            headers : {
                "Authorization" : atoken
            },
            url: api_url + "/getmembersearch?membersearchval=" + encrypted,
        }).then(function successCallback(responses) {

            var decrypted = CryptoJS.AES.decrypt(responses,$rootScope.base64Key,{ iv: $rootScope.iv });
            response = decrypted.toString(CryptoJS.enc.Utf8);
            console.log('decrypted='+ response);

            debugger
            if (response.status = 200) {
                $scope.members = response.data[0];
                if (response.data[0]) {
                    $http({
                        method: 'GET', 
                        headers : {
                            "Authorization" : atoken
                        },
                        url: api_url + "/members/" + $scope.members.memberid,
                    }).then(function successCallback(response) { 

                        $(".input-field label").addClass("active");
                        if (response.status = 200) {
                            $scope.objprofile = response.data[0];
                        }
                    }, function errorCallback(response) {
                        Materialize.toast('Something has gone wrong!', 3000, 'red');
                    });
                } else {
                    Materialize.toast('Texco Number Not Found!', 3000, 'red');
                }
            } else {
                Materialize.toast('Texco Number Not Found!', 3000, 'red');
            }
        }, function errorCallback(response) {
            Materialize.toast('Something has gone wrong!', 3000, 'red');
        });
    }

    $scope.searchmember = function (serviceno) {
        debugger

        var source_string  = 'serviceno';   

        var encrypted = CryptoJS.AES.encrypt(source_string,base64Key,{ iv: iv });
        var cypherString = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
        var cipherParams = CryptoJS.lib.CipherParams.create({ ciphertext: CryptoJS.enc.Base64.parse(cypherString) }); 
        console.log('cypherString',cipherParams);

        $http({
            method: 'GET', 
            headers : {
                "Authorization" : atoken
            },
            url: api_url + "/getmembersearch?membersearchval=" + cipherParams,
        }).then(function successCallback(response) {
            debugger
            var encrypted = response.data;
            var cipherParams = CryptoJS.lib.CipherParams.create({
                ciphertext: CryptoJS.enc.Base64.parse(encrypted)
            });
            var decrypted = CryptoJS.AES.decrypt(cipherParams,$rootScope.base64Key,{ iv: $rootScope.iv });
            response.data = decrypted.toString(CryptoJS.enc.Utf8);
            console.log('decrypted='+ response.data);
            

            if (response.status = 200) {
                // console.log('response', response);
                $scope.members = response.data[0];
                if (response.data[0]) {
                    $http({
                        method: 'GET',
                        headers : {
                            "Authorization" : atoken
                        },
                        url: api_url + "/members/" + $scope.members.memberid,
                    }).then(function successCallback(response) {

                        $(".input-field label").addClass("active");
                        if (response.status = 200) {
                            $scope.objprofile = response.data[0];
                            debugger
                        }
                    }, function errorCallback(response) {
                        Materialize.toast('Something has gone wrong!', 3000, 'red');
                    });
                } else {
                    Materialize.toast('Texco Number Not Found!', 3000, 'red');
                }
            } else {
                Materialize.toast('Texco Number Not Found!', 3000, 'red');
            }
        }, function errorCallback(response) {
            Materialize.toast('Something has gone wrong!', 3000, 'red');
        });
    };

    //Get memberinfo
    $scope.getservicenoexist = function (serviceno) {
        $scope.servicenovalid = '';
        $("#failure").html("");
        $scope.source_string  = serviceno.toString();   
        console.log('$scope.source_string',$rootScope.source_string);
        var encrypted = CryptoJS.AES.encrypt($scope.source_string,$rootScope.base64Key,{ iv: $rootScope.iv });
        $scope.ciphertext = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
        console.log('ciphertext = ' + $scope.ciphertext); 
        $http({
            url: api_url + "/memberinfo?texserno=" + $scope.ciphertext,
            data: $.param({
                texserno: $scope.ciphertext
            }), 
            headers : {
                "Authorization" : atoken
            },
            method: 'GET',
        }).success(function (response, result) {
            if (result = 200) {
                $scope.servicenovalid = "Exist";
                $scope.DisableButton = false;
            }
        }).error(function (error) {

        });
    }

    // Get Selected members
    $scope.select = function (member) {
        $scope.selected = member;
        $scope.objmember.memberid = member.memberid;
        $scope.objmember.accountno = member.accountno;
    }

    // SAVE Account Number - In both apply and member
    $scope.savemember = function (data) {
        debugger
        $scope.isDisabled = true;
        $scope.DisableButton = true;
        var method;
        if (data.memberid > 0) {
            method = 'PUT';
        } else {
            method = 'POST';
        }
        $http({
            url: api_url + "/accountno",
            data: $.param({
                "accountno": data.accountno,
                "memberid": data.memberid
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                "Authorization" : atoken
            },
            method: method,
        }).success(function (response, result) {
            if (result = 200) {
                Materialize.toast('AccountNo Updated Successfully', 3000, 'green');
            }
            else {
                Materialize.toast(error, 3000, 'red');
            }
        }).error(function (error) {
            Materialize.toast(error, 3000, 'red');
        });
    };

});